import asyncio
import pytest
import pytest_aiohttp
import json

from app import init_app


@pytest.fixture
def cli(loop, aiohttp_client):
    app = init_app()
    return loop.run_until_complete(aiohttp_client(app))


async def test_currency_list(cli):
    resp = await cli.get("/currencies")
    assert resp.status == 200
    jsonString = await resp.text()
    jsonObject = json.loads(jsonString)
    assert len(jsonObject["available_currencies"]) > 0


async def test_get_date(cli):
    resp = await cli.get("/dateOfExchangeList")
    assert resp.status == 200
    jsonString = await resp.text()
    jsonObject = json.loads(jsonString)
    assert "exchange_list_date" in jsonObject


async def test_make_exchange(cli):
    resp = await cli.get("/make_exchange?from=CZK&to=EUR&amount=1000")
    assert resp.status == 200
    jsonString = await resp.text()
    jsonObject = json.loads(jsonString)
    assert jsonObject["currency"] == "EUR"
    assert jsonObject["amount"] > 0
