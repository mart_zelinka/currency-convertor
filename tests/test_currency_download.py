import pytest
import asyncio
from currencyConvertor import getCurrencyData


def test_currency_download():
    jsonObj = asyncio.run(getCurrencyData.executeRequests())[0]
    assert isinstance(jsonObj, dict)
    assert "rates" in jsonObj
