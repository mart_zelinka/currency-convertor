import asyncio
import aiohttp
import json
import os
from datetime import datetime

from . import config

URL = "https://openexchangerates.org/api/latest.json?app_id={}".format(
    config.CONFIG["externalAPI_ID"]
)


def processJsonResponse(jsonResp, requiredCurrencies):
    # timestamp of request is our czech ts minus 2hours

    datetimeObj = datetime.fromtimestamp(jsonResp["timestamp"])
    datetimeStr = datetimeObj.strftime("%Y-%m-%d")

    newJson = {"timestamp": jsonResp["timestamp"], "date": datetimeStr, "base": "USD"}

    rates = dict()
    for requiredCurrency in requiredCurrencies:
        if requiredCurrency in jsonResp["rates"]:
            rates[requiredCurrency] = jsonResp["rates"][requiredCurrency]

    newJson["rates"] = rates

    fileName = f"data/exchangeCurrency_{datetimeStr}.json"

    if not os.path.exists(fileName):
        with open(fileName, "w") as f:
            json.dump(newJson, f, indent=4)


async def sendOneRequest(session: aiohttp.ClientSession, url: str):
    response = await session.get(url)
    jsonResponse = await response.json()
    return jsonResponse


async def executeRequests():
    async with aiohttp.ClientSession() as session:
        result = await asyncio.gather(
            sendOneRequest(session, URL), return_exceptions=True
        )
    return result


def download_currencies():
    loop = asyncio.get_event_loop()
    ret = loop.run_until_complete(executeRequests())
    processJsonResponse(ret[0], config.CONFIG["requiredCurrencies"])


def create_file():
    fileContent = {"first": 10, "second": 20}

    timeStr = datetime.today().strftime("%Y%m%d_%H:%M:%S")
    with open(f"data/{timeStr}.json", "w") as f:
        json.dump(fileContent, f, indent=4)
