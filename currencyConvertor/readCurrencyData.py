import os
import json

CURRENCY_DATA = {}


def readCurrencyExchangeData():
    global CURRENCY_DATA

    files = os.listdir("data/")
    files = list(filter(lambda f: "exchangeCurrency" in f, files))
    files.sort(key=lambda f: f)

    with open(os.path.join("data", files[-1]), "r") as f:
        CURRENCY_DATA = json.load(f)


readCurrencyExchangeData()
