from celery import Celery
from celery.schedules import crontab
from celery.result import AsyncResult

from . import getCurrencyData, config

config.loadConfiguration()

celery = Celery(
    "celeryStartup",
    broker=config.CONFIG["celeryBROKER"],
    backend=config.CONFIG["celeryBACKEND"],
)
celery.conf.update(enable_utc=True, timezone="UTC")


@celery.on_after_configure.connect
def setup_periodic_tasks(**kwargs):
    # Testing task call every 10 seconds
    # celery.add_periodic_task(50.0, createFile.s())

    # Set to run on concrete UTC time
    celery.add_periodic_task(
        crontab(hour=1, minute=0, day_of_week="*"), downloadCurrencies.s(),
    )


@celery.task
def downloadCurrencies():
    getCurrencyData.download_currencies()


# testing task
@celery.task
def createFile():
    getCurrencyData.create_file()
