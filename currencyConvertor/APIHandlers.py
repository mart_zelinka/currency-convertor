from aiohttp import web
import json

from .readCurrencyData import CURRENCY_DATA


async def getCurrencyList(request):
    response_obj = {"available_currencies": list(CURRENCY_DATA["rates"].keys())}
    return web.Response(text=json.dumps(response_obj), status=200)


async def makeExchange(request):
    try:
        currencyFrom = request.query["from"]
        currencyTo = request.query["to"]

        amount = int(request.query["amount"])

        # exchange to USD
        usdAmount = amount / CURRENCY_DATA["rates"][currencyFrom]

        # from USD to target currency
        finalAmount = round(usdAmount * CURRENCY_DATA["rates"][currencyTo], 2)

        response_obj = {"currency": currencyTo, "amount": finalAmount}

        return web.Response(text=json.dumps(response_obj), status=200)

    except Exception as e:
        response_obj = {"status": "failed", "message": str(e)}

        return web.Response(text=json.dumps(response_obj), status=500)


async def getDateOfExchangeList(request):
    response_obj = {"exchange_list_date": CURRENCY_DATA["date"]}
    return web.Response(text=json.dumps(response_obj), status=200)
