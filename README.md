# README #

### Currency convertor REST API exposes tree action - all request are of GET type.

#### Make exchange - Computation of currency exchange ####
`http://localhost:8080/make_exchange?from=CZK&to=EUR&amount=2000`

*json result:*

`{"currency": "EUR", "amount": 74.69}`

#### Currencies - Get list of currencies for which app can compute exchange ####
`http://localhost:8080/currencies`

*json result:*

`{"available_currencies": ["USD", "EUR", "CZK", "PLN"]}`

#### DateOfExchangeList - The latest currency exchange rates, app has from external API source ####
`http://localhost:8080/dateOfExchangeList`

*json result:*

`{"exchange_list_date": "2020-06-28"}`

***

## Architecture ##

Main package used for REST API is `aiohttp`. For downloading exchange rates is used Celery periodic task, which run every day at 1:00AM UTC. Exchange rates are then stored in `./data` folder. Celery broker and backend can be specified in `config.yml`. In this config is also possible specify currency symbols we are exposing in app. Config and exchange rates are represented in project as global variables `config.CONFIG` and `readCurrencyData.CURRENCY_DATA`.

#### Flaws of app ####
Due to the deadline I did't satisfied all requirements. When Celery downlaod new exchange rates to `./data` folder, exchange rates are not updated in running REST app. Celery is not included in docker image.

#### Commands for docker startup
App run on **port 8080**. Run commands, when current location is repository:
```
docker build -t currency-convertor:1.0 .
docker run -d -p 8080:8080 currency-convertor:1.0
```

#### Start Celery to set periodic task
Run command, when current location is repository:
```
celery -A currencyConvertor.celeryStartup.celery worker -B --loglevel=info
```