import asyncio
from aiohttp import web

from currencyConvertor import APIHandlers, config, getCurrencyData


def init_app() -> web.Application:
    app = web.Application()
    app.router.add_get("/currencies", APIHandlers.getCurrencyList)
    app.router.add_get("/dateOfExchangeList", APIHandlers.getDateOfExchangeList)
    app.router.add_get("/make_exchange", APIHandlers.makeExchange)

    return app


if __name__ == "__main__":
    # downlaod latest exchangeList
    getCurrencyData.download_currencies()

    # start API on http://0.0.0.0:8080
    web.run_app(init_app())
